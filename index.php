<!DOCTYPE html>
<html>
<head>
    <title>Резюме</title>
</head>
<body>
<?php
$personCV = [ // создаем ассоциативный массив
    'fullName' => 'Hiniyatullin Vladislav Ruslanovich',
    'linkFoto' => "<a href = /hw_4_Hiniyatullin/img1.jpg>Фото</a>",
    'email' => 'wladgin@gmail.com',
    'experience' => [
        'Beetroot' => '1.5 года',
        'Arka-Media' => '1 год',
    ],
    'objectives' => [
        'obj_1' => 'Добиться повышения',
        'obj_2' => 'Решать интересные задачи'
    ]
];

?>

<h1 align="center">Резюме</h1>
<!-- разбиваем на блоки и выводим содержимое массива, предварительно проверив на наличие в нем информации -->
<div><p>ФИО: <?php
        if ($personCV['fullName']) {
            echo $personCV['fullName'];
        }
        ?>
    </p></div>

<div><p><?php
        if ($personCV['linkFoto']) {
            echo $personCV['linkFoto'];
        }
        ?>
    </p></div>

<div><p>Email: <?php
        if ($personCV['email']) {
            echo $personCV['email'];
        }
        ?>
    </p></div>

<div><p>Опыт работы: <br> <?php
        if ($personCV['experience'] && count($personCV['experience']) >= 1) {
            foreach ($personCV['experience'] as $key => $nameCompany) {
                echo $key . ' : ' . $nameCompany . '<br>';
            }
        }
        ?>
    </p></div>

<div><p>Цели: <br> <?php
        if ($personCV['objectives']) {
            foreach ($personCV['objectives'] as $key => $nameObj) {
                echo $nameObj . '<br>';
            }
        }
        ?>
    </p></div>

</body>
</html>